package com.project.tugas5;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import static com.project.tugas5.DBmain.TABLENAME;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ModelViewHolder> {
    Context context;
    ArrayList<Item> itemArrayList =new ArrayList<>();
    SQLiteDatabase sqLiteDatabase;
    //generate constructor

    public ItemAdapter(Context context, int singledata, ArrayList<Item> itemArrayList, SQLiteDatabase sqLiteDatabase) {
        this.context = context;
        this.itemArrayList = itemArrayList;
        this.sqLiteDatabase = sqLiteDatabase;
    }

    @NonNull
    @Override
    public ItemAdapter.ModelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.listlayout,null);
        return new ModelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemAdapter.ModelViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final Item item = itemArrayList.get(position);
        holder.txtfname.setText(item.getFirstname());
        holder.txtlname.setText(item.getLastname());

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putInt("id", item.getId());
                bundle.putString("fname", item.getFirstname());
                bundle.putString("lname", item.getLastname());
                Intent intent=new Intent(context,MainActivity.class);
                intent.putExtra("userdata",bundle);
                context.startActivity(intent);
            }
        });
        //delete row
        holder.delete.setOnClickListener(new View.OnClickListener() {
            DBmain dBmain=new DBmain(context);
            @Override
            public void onClick(View v) {
                sqLiteDatabase=dBmain.getReadableDatabase();
                long delele=sqLiteDatabase.delete(TABLENAME,"id="+ item.getId(),null);
                if (delele!=-1){
                    Toast.makeText(context, "deleted data successfully", Toast.LENGTH_SHORT).show();
                    itemArrayList.remove(position);
                    notifyDataSetChanged();
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return itemArrayList.size();
    }

    public class ModelViewHolder extends RecyclerView.ViewHolder {
        TextView txtfname,txtlname;
        Button edit,delete;
        public ModelViewHolder(@NonNull View itemView) {
            super(itemView);
            txtfname=(TextView)itemView.findViewById(R.id.txtfname);
            txtlname=(TextView)itemView.findViewById(R.id.txtlname);
            edit=(Button)itemView.findViewById(R.id.txt_btn_edit);
            delete=(Button)itemView.findViewById(R.id.txt_btn_delete);
        }
    }
}
