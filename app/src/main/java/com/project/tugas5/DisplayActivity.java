package com.project.tugas5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import java.util.ArrayList;

import static com.project.tugas5.DBmain.TABLENAME;

public class DisplayActivity extends AppCompatActivity {
    DBmain dBmain;
    SQLiteDatabase sqLiteDatabase;
    RecyclerView recyclerView;
    ItemAdapter itemAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        dBmain=new DBmain(this);
        //create method
        findid();
        displayData();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void displayData() {
        sqLiteDatabase=dBmain.getReadableDatabase();
        Cursor cursor=sqLiteDatabase.rawQuery("select *from "+TABLENAME+"",null);
        ArrayList<Item> itemArrayList =new ArrayList<>();
        while (cursor.moveToNext()){
            int id=cursor.getInt(0);
            String fname=cursor.getString(1);
            String lname=cursor.getString(2);
            itemArrayList.add(new Item(id, fname, lname));
        }
        cursor.close();
        itemAdapter =new ItemAdapter(this,R.layout.listlayout, itemArrayList,sqLiteDatabase);
        recyclerView.setAdapter(itemAdapter);
    }

    private void findid() {
        recyclerView=findViewById(R.id.rv);
    }
}